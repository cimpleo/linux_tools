
#!/bin/bash

if [ "$(grep vm.swappiness /etc/sysctl.conf)" != "vm.swappiness=10" ]; then
echo vm.swappiness=10 | sudo tee -a /etc/sysctl.conf
fi
if [ "$(grep vm.vfs_cache_pressure /etc/sysctl.conf)" != "vm.vfs_cache_pressure=1000" ]; then
echo vm.vfs_cache_pressure=1000 | sudo tee -a /etc/sysctl.conf
fi
if [ "$(grep vm.dirty_background_ratio /etc/sysctl.conf)" != "vm.dirty_background_ratio = 10" ]; then
echo vm.dirty_background_ratio = 10 | sudo tee -a /etc/sysctl.conf
fi
if [ "$(grep vm.dirty_ratio /etc/sysctl.conf)" != "vm.dirty_ratio = 40" ]; then
echo vm.dirty_ratio = 40 | sudo tee -a /etc/sysctl.conf
fi
if [ "$(grep vm.dirty_writeback_centisecs /etc/sysctl.conf)" != "vm.dirty_writeback_centisecs = 15000" ]; then
echo vm.dirty_writeback_centisecs = 15000 | sudo tee -a /etc/sysctl.conf
fi