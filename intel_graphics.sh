sudo mkdir /etc/X11/xorg.conf.d/

sudo cat > /etc/X11/xorg.conf.d/20-intel-graphics.conf << EOF

Section "Device"
   Identifier  "Intel Graphics"
   Driver      "intel"
   Option      "TripleBuffer" "true"
   Option      "TearFree"     "true"
   Option      "DRI"          "false"
EndSection
EOF

