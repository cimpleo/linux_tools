#!/bin/bash
sudo pacman-mirrors --geoip && sudo pacman -Syu
sudo pacman -S git code filezilla brave yay base-devel docker-compose
sudo pacman -R midori
yay code-marketplace
yay mattermost-desktop-bin fio
sudo systemctl enable fstrim.timer
fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=test --bs=4k --iodepth=64 --size=4G --readwrite=randrw --rwmixread=75

